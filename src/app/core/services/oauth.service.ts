import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {ApiUrls} from '../api-urls';
import {tap} from 'rxjs/internal/operators';

@Injectable({
  providedIn: 'root'
})
export class OauthService {
  constructor(private http: HttpClient) {}
  login(username: string, password: string) {
    const body = {
      'email': username,
      'password': password
    };
    console.log(username, password);
    return this.http.post( ApiUrls.login, body )
      .pipe(
        tap(
          response => localStorage.setItem('token', response['token'])
        )
      );
  }
  registration(username: string, email: string, password1: number, password2: number) {
    const params = {
      username,
      email,
      password1,
      password2
    };
    // console.log(username, email, password1, password2);
    return this.http.post( ApiUrls.registration, params );
  }
}
