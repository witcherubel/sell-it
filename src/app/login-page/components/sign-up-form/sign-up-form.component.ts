import {Component, EventEmitter, OnInit, Output, ViewEncapsulation} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {OauthService} from '../../../core/services/oauth.service';

@Component({
  selector: 'app-sign-up-form',
  templateUrl: './sign-up-form.component.html',
  styleUrls: ['./sign-up-form.component.scss'],
  // encapsulation: ViewEncapsulation.None
})
export class SignUpFormComponent implements OnInit {

  public signUpForm: FormGroup;
  constructor(private oauth: OauthService) {
    this.signUpForm = new FormGroup({
      userName: new FormControl('', [
        Validators.required
      ]),
      email: new FormControl('', [
        Validators.email,
        Validators.required
      ]),
      password: new FormControl('', Validators.required),
      confirmPassword: new FormControl('', Validators.required)
    });
  }
  @Output() submitSignUp = new EventEmitter();
  handleSubmit() {
    this.submitSignUp.emit();
    console.log(this.signUpForm.value);
    this.oauth.registration(
      this.signUpForm.value.userName,
      this.signUpForm.value.email,
      this.signUpForm.value.password,
      this.signUpForm.value.confirmPassword
    ).subscribe(
      response => console.log(response)
    );
  }
  ngOnInit() {
  }

}
