import {Component, EventEmitter, OnInit, Output, ViewEncapsulation} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {OauthService} from '../../../core/services/oauth.service';

@Component({
  selector: 'app-sign-in-form' ,
  templateUrl: './sign-in-form.component.html',
  styleUrls: ['./sign-in-form.component.scss'],
})
export class SignInFormComponent implements OnInit {
  public signInForm: FormGroup = new FormGroup({
    userEmail: new FormControl('', [
      Validators.required,
      Validators.email
    ]),
    userPassword: new FormControl('', [
      Validators.required,
      Validators.minLength(6),
      Validators.maxLength(12)
    ])
  });
  public userEmail = this.signInForm.get('userEmail');
  public userPassword = this.signInForm.get('userPassword');
  constructor(private oauth: OauthService) {}
  @Output() submitForm = new EventEmitter();
  handleSubmit() {
    this.submitForm.emit();
    this.oauth.login(
      this.signInForm.value.userEmail,
      this.signInForm.value.userPassword
    ).subscribe(
      response => console.log(response)
    );
    // console.log(this.signInForm.value);
  }
  ngOnInit() {
  }

}
